const tasks = angular.module("tasks", []);



tasks
  .directive('tasksPage', function () {
    return {
      scope: {},
      template:/* html */`<div> TasksPage {{$ctrl.title}}
        <div class="row">
          <div class="col">
            <tasks-list tasks="$ctrl.tasks" selected="$ctrl.selected" on-remove="$ctrl.remove($event)"
                        on-select="$ctrl.select($event)"></tasks-list>
          </div>
          <div class="col">
            <task-details task="$ctrl.selected" mode="$ctrl.mode" 
            on-cancel="$ctrl.cancel($event)" on-edit=$ctrl.edit($ctrl.selected) on-save=$ctrl.save($ctrl.selected)>
            </task-details>
            <task-editor task="$ctrl.taskTmp" on-add="$ctrl.add($event)"></task-editor>
          </div>
        </div>
      </div>`,
      controller($scope) {
        const vm = $scope.$ctrl = {}
        vm.title = 'Tasks'
        vm.selected = null
        vm.mode = ""
        vm.taskTmp={}
        vm.tasks = [{ id: 1, name: 'task1' }, { id: 2, name: 'task2' }, { id: 3, name: 'task3' }]
        vm.select = (task) => { vm.selected = { ...task }; vm.mode = "Edit" }
        vm.cancel = (event) => { vm.selected = null; vm.mode = "" }
        vm.edit = (tmpTask) => {
          vm.tasks = vm.tasks.map((elem) =>
            elem.id === tmpTask.id ? tmpTask : elem)
          vm.cancel()
        }
        vm.remove = (selected) => {
          vm.tasks = vm.tasks.filter((elem => elem.id !== selected.id))
        }
        vm.add= (task)=>{
          vm.taskTmp.id = Math.random()*100
          vm.tasks.push(task)
          vm.taskTmp={}
        }
      }
    }
  })
  .directive('tasksList', function () {
    return {
      scope: { tasks: '<', selected: '<', onSelect: "&", onRemove: "&" },
      template:/* html */`<div> List: <div class="list-group">
        <div class="list-group-item d-flex justify-content-between" ng-repeat="task in tasks track by task.id" 
          ng-class="{active: task.id == selected.id}"
          ng-click="onSelect({$event: task})">
        {{$index+1}}. {{task.name}}<button class="btn-close" ng-click="onRemove({$event: task})"></button></div>
      </div></div>`
    }
  })
  .directive('taskDetails', function () {
    return {
      scope: { task: '=', mode: '=', onEdit: '&', onCancel: '&', onSave: '&' },
      template:/* html */`<div>tasksDetails <dl>
        <dt>Name</dt><dd>{{task.name}}</dd>
      </dl>
        <input ng-model=task.name ng-if="task && mode === 'Edit'">
        <button ng-if="mode" ng-click="onEdit({$event:task})&&mode==='Edit'">{{mode}}</button>
        <button ng-if="mode" ng-click="onCancel({$event:task})">Cancel</button>
      </div>`,
    }
  })
  .component('taskEditor', {
    bindings: { task: '<', onAdd: '&' },
    template:/* html */`<div> taskForm:
        <div class="form-group mb-3">
          <label for="task_name">Name</label>
          <input id="task_name" type="text" class="form-control" 
                ng-model="$ctrl.task.name">
        </div>

        <div class="form-group mb-3">
          <label for="task_completed">
            <input id="task_completed" ng-model="$ctrl.task.completed" type="checkbox"> Completed</label>
        </div>

        <button ng-click="$ctrl.onAdd({$event: $ctrl.task})">Add</button>
      </div>`,
    // controller:'TaskEditorCtrl as $ctrl'
    // controller($) {
    //   this.$onInit = () => {
    //     this.taskTmp = { }
    //   }

    //   // https://docs.angularjs.org/guide/component#component-based-application-architecture
    //   // this.$onChanges = (changes) => { this.taskTmp = { ...this.task } }
    //   this.$doCheck = () => { } // after parent $digest
    //   this.$postLink = () => { } // after all child DOM is ready

    //   this.$onDestroy = () => {
    //     console.log('bye bye!')
    //   }
    // },
    // // controllerAs: '$ctrl'
  })

  






















tasks.controller("TasksCtrl", ($scope) => {
  $scope.task = {
    id: "123",
    name: "Task 123 ",
    completed: true,
  };
  $scope.tasks = [
    {
      id: "123",
      name: "Task 123",
      completed: true,
    },
    {
      id: "234",
      name: "Task 234",
      completed: false,
    },
    {
      id: "345",
      name: "Task 345",
      completed: true,
    },
  ];

  $scope.filteredTasks = [...$scope.tasks];
  $scope.filterName = '';




  $scope.selectTask = (itemId) => {
    $scope.task = $scope.tasks.find((elem) => elem.id === itemId);
  };

  $scope.edit = () => {
    $scope.mode = "edit";
    $scope.taskTmp = { ...$scope.task };
  };

  $scope.save = () => {
    $scope.task = { ...$scope.taskTmp };
    $scope.tasks = $scope.tasks.map((elem) =>
      elem.id === $scope.taskTmp.id ? $scope.taskTmp : elem
    );
    $scope.filter();
  };

  $scope.cancel = () => {
    $scope.mode = "hide";
  };

  $scope.remove = (itemId) => {
    $scope.tasks = $scope.tasks.filter((elem) => elem.id !== itemId);
    $scope.task = {};
    $scope.filter();
    // $scope.count();
  };

  $scope.add = () => {
    $scope.mode = "add";
    $scope.taskTmp = { name: "" };
  };
  $scope.addTask = () => {
    $scope.mode = "add";
    $scope.taskTmp.id = Math.random() * 100 + "";
    $scope.tasks.push($scope.taskTmp);
    $scope.add();
    $scope.filter();
    // $scope.count();
  };

  $scope.filter = () => {
    $scope.filteredTasks = $scope.tasks.filter((item) =>
      item.name.toLowerCase().includes($scope.filterName.toLowerCase())
    );
    //    $scope.count();
  };

  $scope.enterPress = (e) => {
    if (e.which === 13 && $scope.mode === "add") {
      $scope.addTask();

    }
    if (e.which === 13 && $scope.mode === "edit") {
      $scope.save();
      $scope.cancel();
    }
  };

  $scope.count = () => {
    $scope.completed = 0;
    $scope.uncompleted = 0;
    $scope.tasks.forEach(element => {
      if (element.completed) {
        $scope.completed++;
      } else {
        $scope.uncompleted++;
      }
    });
  }
  $scope.count();

  $scope.$watchCollection('tasks', (oldVal, newVal) => {
    $scope.count()
  })
});
