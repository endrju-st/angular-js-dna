const settings = angular.module("settings", []);
settings.controller("SettingsCtrl", ($scope) => {
  const vm = ($scope.settings = {});

  vm.test = "123";
});

settings.directive("appHighlight", function () {
  return {
    restrict: "ECMA",

    link(scope, $element, attrs, ctrl) {
      console.log("Hello", $element);

      scope.$watch("settings.title", () => {
        $element.html("<p> " + scope.settings.title + " &raquo; &nbsp; </p>");
      });

      $element
        .css("color", "red")
        .html("<p>scope.Settings.title</p>")
        .on("click", (event) => {
          angular.element(event.currentTarget).css("color", "blue");
        });
    },
  };
});

settings.directive("appAlert", function () {
  return {
    scope: {
      message: "@",
      onDismiss: "&",
      type: "@",
    },
    template: /*html*/ `<div class="alert alert-{{$ctrl.type}}" ng-if="$ctrl.open">
        <span class="close float-end" ng-click="$ctrl.dismiss()">&times;</span>
        <span> {{$ctrl.message}} </span>
        </div>`,
    bindToController: true,
    controllerAs: "$ctrl",

    controller() {
      // const vm = $scope.$ctrl = {}
      this.open = true;
      this.dismiss = () => {
        this.open = false;
        this.onDismiss();
      };
    },
  };
});
