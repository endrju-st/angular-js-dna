const users = angular.module("users", ["usersService"]);

users.component('usersPage', {
  template: /*html */`
<div class="row">
     <div class="col">
          <users-list users="$ctrl.users" selected="$ctrl.selected" on-select=$ctrl.select($event)
          on-delete=$ctrl.delete($event)></users-list>
          <users-details selected="$ctrl.selected" ng-if="$ctrl.open" on-save="$ctrl.save($event)"></users-details>
          <user-add on-add="$ctrl.add($event)"></user-add>
      </div>
</div>
          `,
  controller(UsersService) {

    this.open = false
    this.selected = {}

    // this.users = [{ id: 1, name: 'Ala' }, { id: 2, name: 'Ola' }, { id: 3, name: 'Ela' }]
    this.select = (user) => {
      this.selected = user
      this.open = true
    };
    this.delete = (user) => {
      UsersService.deleteUser(user).then(() => {
        this.refresh();
      });

    };
    this.refresh = () => {
      UsersService.fetchUsers().then((data) => {
        this.users = data;
      });
    };
    this.refresh()
    this.add = (newUser) => {
      UsersService.addUser(newUser).then(() => {
        this.refresh();
      });
    }
    this.save=(tmpUser)=>{
      console.log("object");
      UsersService.updateUser(tmpUser).then(() => {
        this.refresh();
        this.selected={}
      });
    }
  }

})
users.component('usersList', {
  bindings: { users: '<', selectedInList: '<selected', onSelect: '&', onDelete: '&' },
  template: /*html */`<div class="list-group" >
  <div class="list-group-item d-flex justify-content-between" ng-click="$ctrl.onSelect({$event:user})"
  ng-class="{active: user.id == $ctrl.selectedInList.id}" ng-repeat="user in $ctrl.users">
      {{user.username}}
      <button type="button" class="close" aria-label="Close" >
      <span ng-click="$ctrl.onDelete({$event:user})" aria-hidden="true">&times;</span>
      </button>
      </div>`

})
users.component('usersDetails', {
  bindings: { selected: '<', onSave: '&'},
  template: /*html */`<div class="col-5">
  <div class ="row">
  <h4>Selected user details</h4>
  <dl>
      <dt>{{$ctrl.selected.id}}</dt>
      <dd>{{$ctrl.selected.username}}</dd>
  </dl>
  <input ng-model="$ctrl.selected.username">
  <button  ng-click="$ctrl.onSave({$event : $ctrl.selected})">SAVE</button>
</div>`,
  })
  users.component('userAdd', {
    bindings: { onAdd: '&'},
    template: /*html */`<div class="col-5">
    <h4>Add User</h4>
    <input ng-model="newuser.username">
    <button  ng-click="$ctrl.onAdd({$event : newuser})">ADD</button>
  </div>`,
    })















users.controller("UsersPageCtrl", ($scope, UsersService, $http, $timeout) => {
  const vm = ($scope.usersPage = {});
  // vm.users = UsersService.getUsers()
  vm.selectedUser = null;
  vm.userTmp;

  vm.select = (user) => {
    UsersService.getUsersById(user).then((resp) => {
      vm.selectedUser = resp.data;
      vm.userTmp = { ...vm.selectedUser };
    });
    vm.mode = "editUser";
  };
  vm.edit = () => {
    vm.mode = "editUser";
    vm.userTmp = { ...vm.selectedUser };
  };

  vm.save = (draft) => {
    if (vm.form.$invalid) {
      vm.showMessage("form is invalid")
      return
    }
    UsersService.updateUser(draft).then(() => {
      vm.refresh();
    });
    // vm.select(saved.id)
  };

  vm.showMessage = (msg) => {
    vm.alert = alert(msg)
    vm.alert
    // $timeout(()=>{ 
    //   vm.alert = alert(msg);
    //   vm.alert;

    //   window.close()},2000)
  }

  vm.add = () => {
    vm.mode = "addUser";
    vm.newUser = {};
  };

  vm.addNewUser = (user) => {
    UsersService.addUser(user).then(() => {
      vm.refresh();
    });
  };

  vm.refresh = () => {
    UsersService.fetchUsers().then((data) => {
      vm.users = data;
    });
  };

  vm.delete = (user) => {
    UsersService.deleteUser(user).then(() => {
      vm.refresh();
    });
  };

  vm.refresh();
});
