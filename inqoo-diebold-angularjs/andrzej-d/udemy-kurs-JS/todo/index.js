let todoInput, errorInfo, addBtn, ulList, newTodo;
let popup, popupInfo, todoToEdit, popupInput, popupAddBtn, popupCloseBtn;

const main = () => {
  prepareDOMElements();
  prepareDOMEvents();
};

const prepareDOMElements = () => {
  todoInput = document.querySelector(".todo-input");
  errorInfo = document.querySelector(".error-info");
  addBtn = document.querySelector(".btn-add");
  ulList = document.querySelector(".todolist ul");
  popupInfo = document.querySelector(".popup-info");
  popupInput = document.querySelector(".popup-input");
  popupAddBtn = document.querySelector(".accept");
  popupCloseBtn = document.querySelector(".cancel");
  popup = document.querySelector(".popup");
};

const prepareDOMEvents = () => {
  addBtn.addEventListener("click", addNewTask);
  ulList.addEventListener("click", checkClick);
  popupCloseBtn.addEventListener("click", closePopup);
  popupAddBtn.addEventListener("click", changeTodoText);
  todoInput.addEventListener("keydown", (event) => {
    if (event.which === 13) {
      addNewTask();
    }
  });
};

const addNewTask = () => {
  if (todoInput.value) {
    newTodo = document.createElement("li");
    newTodo.textContent = todoInput.value;
    newTodo.append(addToolsToTask());
    ulList.append(newTodo);
    todoInput.value = "";
    errorInfo.textContent = "";
  } else {
    errorInfo.textContent = " Wpisz tresc zadania!!!";
  }
};

const addToolsToTask = () => {
  let toolsPanel = document.createElement("div")
  toolsPanel.classList.add("tools");

  let btnComplete = document.createElement("button");
  btnComplete.classList.add("complete");

  let iCheck = document.createElement("i");
  iCheck.classList.add("fas", "fa-check");
  btnComplete.append(iCheck);

  let btnEdit = document.createElement("button");
  btnEdit.classList.add("edit");
  btnEdit.textContent = "EDIT";

  let btnDelete = document.createElement("button");
  btnDelete.classList.add("delete");
  // btnDelete.innerHTML='<i class= fas fa-times></i>'
  let iDelete = document.createElement("i");
  iDelete.classList.add("fas", "fa-times");
  btnDelete.append(iDelete);

  toolsPanel.append(btnComplete, btnEdit, btnDelete);
  return toolsPanel;
};

const checkClick = (e) => {
  if (e.target.matches(".complete")) {
    e.target.closest("li").classList.toggle("completed");
    e.target.classList.toggle("completed");
  } else if (e.target.matches(".edit")) {
    editTodo(e);
  } else if (e.target.matches(".delete")) {
    deleteTodo(e);
  }
};

const editTodo = (e) => {
  todoToEdit = e.target.closest("li");
  popupInput.value = todoToEdit.firstChild.textContent;
  popup.style.display = "flex";
};

const changeTodoText = () => {
  if (popupInput.value) {
    todoToEdit.firstChild.textContent = popupInput.value;
    closePopup();
    popupInfo.textContent = "";
  } else {
    popupInfo.textContent = "Musisz podac jakas tresc";
  }
};

const closePopup = () => {
  popup.style.display = "none";
  popupInfo.textContent = "";
};

const deleteTodo = (e) => {
  e.target.closest("li").remove();
  const allTodos = ulList.querySelectorAll("li");
  if (allTodos.length === 0) errorInfo.textContent = "Brak zadan na liscie";
};

document.addEventListener("DOMContentLoaded", main);
